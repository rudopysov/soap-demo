package org.client.demo;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;

@Configuration
public class SoapConfiguration {

    @Value("http://localhost:8081/ws")
    private String serverUrl;

    @Bean
    public PaymentDocumentsClient paymentDocumentsClient() {
        Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
        marshaller.setContextPath("org.client.demo.wsdl");

        PaymentDocumentsClient client = new PaymentDocumentsClient();
        client.setDefaultUri(serverUrl);
        client.setMarshaller(marshaller);
        client.setUnmarshaller(marshaller);
        return client;
    }

}