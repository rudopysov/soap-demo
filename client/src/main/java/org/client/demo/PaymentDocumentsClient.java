package org.client.demo;

import lombok.extern.log4j.Log4j2;
import org.client.demo.wsdl.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;

import java.math.BigInteger;

@Log4j2
@Component
public class PaymentDocumentsClient extends WebServiceGatewaySupport {

    @Value("http://localhost:8081/ws/paymentDocuments")
    private String serverUrl;

    public GetPaymentDocumentResponse getPaymentDocument(BigInteger id) {

        GetPaymentDocumentRequest request = new GetPaymentDocumentRequest();
        request.setId(id);

        log.debug("Requesting document by id " + id);

        return (GetPaymentDocumentResponse) getWebServiceTemplate()
                .marshalSendAndReceive(serverUrl, request);
    }

    public SavePaymentDocumentResponse savePaymentDocument(PaymentDocument paymentDocument) {

        SavePaymentDocumentRequest request = new SavePaymentDocumentRequest();
        request.setPaymentDocument(paymentDocument);

        log.debug("Save document with id " + paymentDocument.getId());

        return (SavePaymentDocumentResponse) getWebServiceTemplate()
                .marshalSendAndReceive(serverUrl, request);
    }

    public DeletePaymentDocumentResponse deletePaymentDocument(BigInteger id) {

        DeletePaymentDocumentRequest request = new DeletePaymentDocumentRequest();
        request.setId(id);

        log.debug("Delete document with id " + id);

        return (DeletePaymentDocumentResponse) getWebServiceTemplate()
                .marshalSendAndReceive(serverUrl, request);
    }

    public GetAllPaymentDocumentsResponse getAllPaymentDocuments() {
        GetAllPaymentDocumentsRequest request = new GetAllPaymentDocumentsRequest();

        log.debug("Get all documents");

        return (GetAllPaymentDocumentsResponse) getWebServiceTemplate()
                .marshalSendAndReceive(serverUrl, request);
    }

}