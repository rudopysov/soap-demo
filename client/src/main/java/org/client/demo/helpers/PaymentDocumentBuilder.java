package org.client.demo.helpers;

import org.client.demo.wsdl.PaymentDocument;

import java.math.BigDecimal;
import java.math.BigInteger;

public class PaymentDocumentBuilder {

    private BigInteger id;
    private String paymentPurpose;
    private BigDecimal amount;
    private String debtAccount;
    private String creditAccount;

    public PaymentDocumentBuilder withId(BigInteger id) {
        this.id = id;
        return this;
    }

    public PaymentDocumentBuilder withPaymentPurpose(String paymentPurpose) {
        this.paymentPurpose = paymentPurpose;
        return this;
    }

    public PaymentDocumentBuilder withAmount(BigDecimal amount) {
        this.amount = amount;
        return this;
    }

    public PaymentDocumentBuilder withDebtAccount(String debtAccount) {
        this.debtAccount = debtAccount;
        return this;
    }

    public PaymentDocumentBuilder withCreditAccount(String creditAccount) {
        this.creditAccount = creditAccount;
        return this;
    }

    public PaymentDocument build() {
        PaymentDocument document = new PaymentDocument();
        document.setId(id);
        document.setPaymentPurpose(paymentPurpose);
        document.setAmount(amount);
        document.setDebtAccount(debtAccount);
        document.setCreditAccount(creditAccount);
        return document;
    }
}
