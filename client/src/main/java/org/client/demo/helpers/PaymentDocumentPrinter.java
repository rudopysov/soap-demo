package org.client.demo.helpers;

import lombok.NonNull;
import org.client.demo.wsdl.PaymentDocument;

public class PaymentDocumentPrinter {

    private PaymentDocumentPrinter() {
    }

    public static String documentToString(@NonNull PaymentDocument document) {
        return String.format("Document id = %s\n" +
                        "Amount = %s\n" +
                        "PaymentPurpose = %s\n" +
                        "DebtAccount = %s\n" +
                        "CreditAccount = %s",
                document.getId().toString(),
                document.getAmount().toString(),
                document.getPaymentPurpose(),
                document.getDebtAccount(),
                document.getCreditAccount());
    }
}
