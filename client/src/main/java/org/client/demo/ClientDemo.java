package org.client.demo;

import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.client.demo.helpers.PaymentDocumentBuilder;
import org.client.demo.wsdl.GetPaymentDocumentResponse;
import org.client.demo.wsdl.PaymentDocument;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Component;
import org.springframework.ws.soap.client.SoapFaultClientException;

import java.math.BigDecimal;
import java.math.BigInteger;

import static org.client.demo.helpers.PaymentDocumentPrinter.documentToString;

@Component
@RequiredArgsConstructor
@Log4j2
public class ClientDemo {

    private final PaymentDocumentsClient client;

    public static void main(String[] args) {
        ApplicationContext ctx = new AnnotationConfigApplicationContext("org.client.demo");

        ClientDemo main = ctx.getBean(ClientDemo.class);

        PaymentDocument firstDocument = new PaymentDocumentBuilder()
                .withId(BigInteger.ZERO)
                .withPaymentPurpose("Payment purpose")
                .withAmount(BigDecimal.TEN)
                .withCreditAccount("1234567890")
                .withDebtAccount("0987654321")
                .build();

        PaymentDocument secondDocument = new PaymentDocumentBuilder()
                .withId(BigInteger.ONE)
                .withAmount(BigDecimal.TEN)
                .withCreditAccount("0987654321")
                .withDebtAccount("1234567890")
                .build();

        log.info("First Document save successful: " + main.client.savePaymentDocument(firstDocument).isSuccessful());

        GetPaymentDocumentResponse firstDocumentResponse = main.client.getPaymentDocument(BigInteger.ZERO);
        log.info("Received document: " + documentToString(firstDocumentResponse.getPaymentDocument()));

        log.info("Second Document save successful: " + main.client.savePaymentDocument(secondDocument).isSuccessful());

        GetPaymentDocumentResponse secondDocumentResponse = main.client.getPaymentDocument(BigInteger.ONE);
        log.info("Received document: " + documentToString(secondDocumentResponse.getPaymentDocument()));

        log.info("Document repository now contains " + main.client.getAllPaymentDocuments().getPaymentDocument().size() + " documents");

        log.info("First Document delete successful: " + main.client.deletePaymentDocument(firstDocument.getId()).isSuccessful());

        log.info("Document repository now contains " + main.client.getAllPaymentDocuments().getPaymentDocument().size() + " documents");

        log.info("Second Document delete successful: " + main.client.deletePaymentDocument(secondDocument.getId()).isSuccessful());

        log.info("Document repository now contains " + main.client.getAllPaymentDocuments().getPaymentDocument().size() + " documents");

        PaymentDocument brokenDocument = new PaymentDocumentBuilder()
                .withId(BigInteger.ONE)
                .withAmount(BigDecimal.TEN)
                .withCreditAccount("11")
                .withDebtAccount("12")
                .build();

        try {
            log.info("Try to save broken document...");
            log.info("Broken Document save successful: " + main.client.savePaymentDocument(brokenDocument).isSuccessful());
        } catch (SoapFaultClientException e) {
            log.error(e);
        }
    }
}
