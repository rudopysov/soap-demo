package org.server.demo;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.ws.config.annotation.EnableWs;
import org.springframework.ws.config.annotation.WsConfigurerAdapter;
import org.springframework.ws.server.EndpointInterceptor;
import org.springframework.ws.soap.server.endpoint.interceptor.PayloadValidatingInterceptor;
import org.springframework.ws.transport.http.MessageDispatcherServlet;
import org.springframework.ws.wsdl.wsdl11.DefaultWsdl11Definition;
import org.springframework.xml.xsd.SimpleXsdSchema;
import org.springframework.xml.xsd.XsdSchema;

import javax.servlet.ServletContext;
import javax.servlet.ServletRegistration;
import java.util.List;

@Configuration
@EnableWs
@ComponentScan(basePackages = {"org.server.demo"})
public class SpringAppConfig extends WsConfigurerAdapter implements WebApplicationInitializer {

    @Override
    public void onStartup(ServletContext container) {
        // Create the 'root' Spring application context
        AnnotationConfigWebApplicationContext rootContext = new AnnotationConfigWebApplicationContext();
        rootContext.register(SpringAppConfig.class);

        // Manage the lifecycle of the root application context
        container.addListener(new ContextLoaderListener(rootContext));

        // Create the dispatcher servlet's Spring application context
        AnnotationConfigWebApplicationContext dispatcherContext = new AnnotationConfigWebApplicationContext();
        MessageDispatcherServlet servlet = new MessageDispatcherServlet(dispatcherContext);
        servlet.setTransformWsdlLocations(true);

        // Register and map the dispatcher servlet
        ServletRegistration.Dynamic dispatcher = container
                .addServlet("dispatcher", servlet);
        dispatcher.setLoadOnStartup(1);
        dispatcher.addMapping("/ws/*");
    }

    @Override
    public void addInterceptors(List<EndpointInterceptor> interceptors) {
        interceptors.add(getValidatingInterceptor());
    }

    @Bean(name = "validatingInterceptor")
    public PayloadValidatingInterceptor getValidatingInterceptor() {
        PayloadValidatingInterceptor validatingInterceptor = new PayloadValidatingInterceptor();
        validatingInterceptor.setSchema(new ClassPathResource("PaymentDocuments.xsd"));
        validatingInterceptor.setValidateRequest(true);
        validatingInterceptor.setValidateResponse(true);
        return validatingInterceptor;
    }

    @Bean(name = "paymentDocuments")
    public DefaultWsdl11Definition defaultWsdl11Definition(XsdSchema paymentDocumentsSchema) {
        DefaultWsdl11Definition wsdl11Definition = new DefaultWsdl11Definition();
        wsdl11Definition.setPortTypeName("PaymentDocumentsPort");
        wsdl11Definition.setLocationUri("/ws");
        wsdl11Definition.setTargetNamespace("http://demo.server.org/service");
        wsdl11Definition.setSchema(paymentDocumentsSchema);
        return wsdl11Definition;
    }

    @Bean
    public XsdSchema paymentDocumentsSchema() {
        return new SimpleXsdSchema(new ClassPathResource("PaymentDocuments.xsd"));
    }
}