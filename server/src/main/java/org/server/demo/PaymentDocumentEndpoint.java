package org.server.demo;

import lombok.RequiredArgsConstructor;
import org.server.demo.service.*;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

@Endpoint
@RequiredArgsConstructor
public class PaymentDocumentEndpoint {
    private static final String NAMESPACE_URI = "http://demo.server.org/service";

    private final PaymentDocumentRepository repository;

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getPaymentDocumentRequest")
    @ResponsePayload
    public GetPaymentDocumentResponse getPaymentDocument(@RequestPayload GetPaymentDocumentRequest request) {
        GetPaymentDocumentResponse response = new GetPaymentDocumentResponse();
        response.setPaymentDocument(repository.findDocument(request.getId()));
        return response;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "savePaymentDocumentRequest")
    @ResponsePayload
    public SavePaymentDocumentResponse savePaymentDocument(@RequestPayload SavePaymentDocumentRequest request) {
        SavePaymentDocumentResponse response = new SavePaymentDocumentResponse();
        Boolean result = repository.save(request.getPaymentDocument());
        response.setSuccessful(result);
        return response;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "deletePaymentDocumentRequest")
    @ResponsePayload
    public DeletePaymentDocumentResponse deletePaymentDocument(@RequestPayload DeletePaymentDocumentRequest request) {
        DeletePaymentDocumentResponse response = new DeletePaymentDocumentResponse();
        Boolean result = repository.delete(request.getId());
        response.setSuccessful(result);
        return response;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getAllPaymentDocumentsRequest")
    @ResponsePayload
    public GetAllPaymentDocumentsResponse getPaymentDocument(@RequestPayload GetAllPaymentDocumentsRequest request) {
        GetAllPaymentDocumentsResponse response = new GetAllPaymentDocumentsResponse();
        response.getPaymentDocument().addAll(repository.getAll());
        return response;
    }
}