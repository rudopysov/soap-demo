package org.server.demo;

import lombok.NonNull;
import org.server.demo.service.PaymentDocument;
import org.springframework.stereotype.Component;

import java.math.BigInteger;
import java.util.Collection;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Component
public class PaymentDocumentRepository {
    private static final Map<BigInteger, PaymentDocument> documents = new ConcurrentHashMap<>();

    public PaymentDocument findDocument(@NonNull BigInteger id) {
        return documents.get(id);
    }

    public Boolean save(@NonNull PaymentDocument paymentDocument) {
        if (documents.containsKey(paymentDocument.getId())) return false;
        documents.put(paymentDocument.getId(), paymentDocument);
        return true;
    }

    public Boolean delete(@NonNull BigInteger id) {
        if (!documents.containsKey(id)) return false;
        documents.remove(id);
        return true;
    }

    public Collection<PaymentDocument> getAll() {
        return documents.values();
    }
}